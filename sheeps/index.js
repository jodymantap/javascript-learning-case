function warnTheSheep(queue) {
    // // your code here
    // let arraySheeps = queue
    // let wolf;
    // let data;
    // let lengthArray = arraySheeps.length;

    // arraySheeps.forEach((val, i, arr) => {
    //     if (val == "wolf") {
    //         wolf = arraySheeps.indexOf(val);
    //     }
    // });

    // if (wolf == lengthArray-1) {
    //     data = "Pls go away and stop eating my sheep"
    // } else {
    //     data = `Oi! Sheep number ${lengthArray-wolf-1}! You are about to be eaten by a wolf!`;
    // }

    // console.log(data);
    // return data;

    queue.reverse();

    if (queue[0] == "wolf") {
        return `Pls go away and stop eating my sheep`;
    } else {
        return `Oi! Sheep number ${queue.indexOf('wolf')}! You are about to be eaten by a wolf!`;
    }
}


// Test Function do not edit
function Test(fun, result) {
    console.log(fun === result)
}


// Test assertions
Test(warnTheSheep(["sheep", "sheep", "sheep", "wolf", "sheep"]), "Oi! Sheep number 1! You are about to be eaten by a wolf!");
Test(warnTheSheep(["sheep", "sheep", "wolf"]), "Pls go away and stop eating my sheep");
Test(warnTheSheep(["sheep", "wolf", "sheep"]), "Oi! Sheep number 1! You are about to be eaten by a wolf!");
Test(warnTheSheep(["wolf", "sheep", "sheep", "sheep", "sheep", "sheep", "sheep"]), "Oi! Sheep number 6! You are about to be eaten by a wolf!");