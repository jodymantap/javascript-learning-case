async function reverseMyName(str) {
    // put your code here

    let temp = await str.split("");
    let arrayReverse = await temp.reverse();
    let joinArray = arrayReverse.join("");

    return joinArray;
}

const Test = (fun, result) => console.log(reverseMyName(fun) === result)

Test("A", "A")
Test("Michael Jackson","noskcaJ leahciM")
Test("Alvian Zachry Faturrahman", "namharrutaF yrhcaZ naivlA")
Test("", "")